//
//  ViewController.swift
//  UIshadowExample
//
//  Created by henrylai on 12/6/19.
//  Copyright © 2019 com.henrylai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
// add commit 9
// add commit 10
    @IBOutlet weak var vwContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        vwContainer.layer.cornerRadius = 10.0
//        vwContainer.layer.shadowColor = UIColor.blue.cgColor
//        vwContainer.layer.shadowOffset = .zero
//        vwContainer.layer.shadowOpacity = 0.6
//        vwContainer.layer.shadowRadius = 15.0
//        vwContainer.layer.shadowPath = UIBezierPath(rect: vwContainer.bounds).cgPath
//        vwContainer.layer.shouldRasterize = true
        
        let width: CGFloat = 200
        let height: CGFloat = 200
        
        let vw = UIImageView(frame: CGRect(x:0, y:0, width: width, height: height))
        vw.image = UIImage(named: "YourPictureHere")
        vw.center = view.center
        view.addSubview(vw)
        vw.backgroundColor = .cyan
        vw.layer.shadowPath = UIBezierPath(rect: vw.bounds).cgPath
        vw.layer.shadowRadius = 5
        vw.layer.shadowOffset = CGSize(width: 0.2, height: 0.5)
        vw.layer.shadowOpacity = 1
        
        
        
    }


}

